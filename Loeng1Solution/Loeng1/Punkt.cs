﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Punkt
    {
        private double _x;
        private double _y;

        public Punkt(double x, double y)
        {
            this._x = x;
            this._y = y;
        }

        public double kaugusNullist()
        {
            return Math.Sqrt(Math.Pow(_x, 2) + Math.Pow(_y, 2));
        }

        public string teataAndmed()
        {
            return "Punkt: (" + _x + "; " + _y + ")";
        }

        public double kaugusTeisestPunktist(Punkt punkt)
        {
            double xVektor = punkt._x - _x;
            double yVektor = punkt._y - _y;

            return Math.Sqrt(Math.Pow(xVektor, 2) + Math.Pow(yVektor, 2));
        }

        public bool kasOnAlgusPunkt()
        {
            return _x == 0 & _y == 0;
        }
    }
}
