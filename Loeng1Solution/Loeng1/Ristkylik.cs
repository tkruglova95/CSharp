﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Ristkylik
    {
        private double _pikkus;
        private double _laius;
        
        public Ristkylik(double pikkus, double laius)
        {
            this._pikkus = pikkus;
            this._laius = laius;
        }

        public double arvutaPindala()
        {
            return _pikkus * _laius;
        }

        public double arvutaYmbermoot()
        {
            return 2 * (_pikkus + _laius);
        }

        public bool kasOnVordsed(Ristkylik ristkylik)
        {
            bool samaPindala = this.arvutaPindala() == ristkylik.arvutaPindala();
            bool samaYmbermoot = this.arvutaYmbermoot() == ristkylik.arvutaYmbermoot();
            return (samaPindala && samaYmbermoot);
        }

        public bool kasOnRuut()
        {
            return (_pikkus == _laius);
        }
    }
}
