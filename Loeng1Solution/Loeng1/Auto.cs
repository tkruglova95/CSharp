﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Auto : Mootorsõiduk
    {
        protected bool _AvatudUksed = false;

        public Auto() : base(100)
        {
        }

        public Auto(int maxkiirus) : base(maxkiirus)
        {
        }

        public void AvaUksed()
        {
            Stop();
            _AvatudUksed = true;
        }

        public void SulgeUksed()
        {
            _AvatudUksed = false;
        }

        public override void Kiirenda(int kiirus)
        {
            if(!_AvatudUksed)
            {
                base.Kiirenda(kiirus);
            }
        }

        public override string Hetkeseis()
        {
            return string.Format("AvatudUksed - {0}, Kiirus - {1}.", _AvatudUksed, _Kiirus);
        }
    }
}
