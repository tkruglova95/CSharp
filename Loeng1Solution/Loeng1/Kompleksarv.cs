﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Kompleksarv
    {
        private double _reaalosa;
        private double _imaginaarosa;
        public Kompleksarv(double reaalosa, double imaginaarosa)
        {
            this._reaalosa = reaalosa;
            this._imaginaarosa = imaginaarosa;
        }
        public Kompleksarv liida(Kompleksarv kompl)
        {
            double uusReaalosa = this._reaalosa + kompl._reaalosa;
            double uusImaginaarosa = this._imaginaarosa + kompl._imaginaarosa;
            return new Kompleksarv(uusReaalosa, uusImaginaarosa);
        }

        public Kompleksarv lahuta(Kompleksarv kompl)
        {
            kompl._reaalosa = kompl._reaalosa * (-1);
            kompl._imaginaarosa = kompl._imaginaarosa * (-1);
            return liida(kompl);
        }

        public override string ToString()
        {
            return string.Format("{0} + {1}i", _reaalosa, _imaginaarosa);
        }
    }
}
