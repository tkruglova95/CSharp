﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Mootorsõiduk
    {
        protected int _Kiirus = 0;
        protected int _MaxKiirus = 20;

        public Mootorsõiduk(int maxKiirus)
        {
            _MaxKiirus = maxKiirus;
        }

        public void Kiirenda()
        {
            _Kiirus += 10;
        }

        public virtual void Kiirenda(int kiirus)
        {
            _Kiirus += kiirus;
            if(_Kiirus > _MaxKiirus)
            {
                _Kiirus = _MaxKiirus;
            }
        }

        public void Stop()
        {
            _Kiirus = 0;
        }

        public virtual string Hetkeseis()
        {
            return string.Format("Kiirus - {0}.", _Kiirus);
        }

        public new void ToString()
        {
            Console.WriteLine(Hetkeseis());
        }
    }
}
