﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Veoauto : Auto
    {
        protected bool _Kallutab = false;

        public Veoauto() : base(70)
        {
        }

        public void HakkaKallutama()
        {
            Stop();
            _Kallutab = true;
        }

        public override void Kiirenda(int kiirus)
        {
            if (!_Kallutab)
            {
                base.Kiirenda(kiirus);
            }
        }

        public override string Hetkeseis()
        {
            return string.Format("Kallutab - {0}, AvatudUksed - {1}, Kiirus - {2}.", _Kallutab, _AvatudUksed, _Kiirus);
        }
    }
}
