﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ulesanded1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(tagurpidi("Tere, maailm!"));
            Console.ReadLine();
        }

    
        static int sisestaNumber()
        {
           
            while(true)
            {
                Console.WriteLine("Sisesta number: ");
                string sisend = Console.ReadLine();

                int num;
                if (int.TryParse(sisend, out num))
                {
                    return num;
                }
            }
            
        }

        static int sisestaNumberRec()
        {

            Console.WriteLine("Sisesta number: ");
            string sisend = Console.ReadLine();

            int num;
            if (int.TryParse(sisend, out num))
            {
                return num;
            } else
            {
                return sisestaNumberRec();
            }

        }

        static double fahrenheitToKelvin(double fahr)
        {
            return (fahr + 459.69) * 5 / 9;

        }

        static double celsiusToFahrenheit(double cel)
        {
            return cel * 9 / 5 + 32;

        }

        static bool onTaisealine(int vanus)
        {
            return vanus >= 18;

        }

        static bool onKolmnurk(int a, int b, int c)
        {
            if (a + b > c && b + c > a && a + c > b)
            {
                return true;
            }
            return false;
        }

        static void sisestaTegevus()
        {
            Console.WriteLine("Mis funktsionaalsust soovid kasutada: ");
            Console.WriteLine("sisestaNumber=1\n" + 
                "sisestaNumberRec=2\n" + 
                "fahrenheitToKelvin=3\n" + 
                "celsiusToFahrenheit=4\n" +
                "onTaisealine=5\n" + 
                "onKolmnurk=6\n");
            string sisend = Console.ReadLine();

            switch (sisend)
            {
                case "1":
                    Console.WriteLine(sisestaNumber());
                    break;
                case "2":
                    Console.WriteLine(sisestaNumberRec());
                    break;
                case "3":
                    Console.WriteLine(fahrenheitToKelvin(100));
                    break;
                case "4":
                    Console.WriteLine(celsiusToFahrenheit(100));
                    break;
                case "5":
                    Console.WriteLine(onTaisealine(22));
                    break;
                case "6":
                    Console.WriteLine(onKolmnurk(1, 2, 3));
                    break;
            }
        }

        static int sonadeArvLauses(string tekst)
        {
            List<string> sonad = new List<string>();
            sonad.AddRange(tekst.Split(' '));

            return sonad.Count;
        }

        static string tagurpidi(string tekst)
        {
            string tagurpidiTekst = "";
            
            for(int i = tekst.Length - 1; i >= 0; i--)
            {
                tagurpidiTekst += tekst[i];
            }

            return tagurpidiTekst;
        }
    }
}
